module gitee.com/burybell/images-bed

go 1.17

require (
	github.com/beego/beego/v2 v2.0.1
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/httprate v0.5.1
	github.com/syndtr/goleveldb v1.0.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
