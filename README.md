# 图床

## 功能
1. 用于私人保存图片
2. 作为typore等markdown编辑器的在线图床
3. 在线图片预览

## 使用方法
### 服务端
```shell
git clone https://gitee.com/burybell/images-bed.git
cd images-bed
chmod +x make.sh
cd bin
./images-fs &
```
> 注意

1. 默认在8080端口启动
2. --base= 可以指定数据保存路径
3. --addr=0.0.0.0:8848 可以指定绑定ip和端口
4. --types=jpeg,jpg,png 可以指定可上传的文件类型后缀

### GO SDK
#### 依赖获取
```shell
go mod init
go get gitee.com/burybell/images-bed
go mod tidy
go mod vendor
```

#### 例子
```go
package main

import (
	"fmt"
	"gitee.com/burybell/images-bed/sdk"
)

func main()  {

	client := sdk.NewImagesBedClient("http", "127.0.0.1", 8848)

	scene := client.CreateScene("bed")

	if scene {
		fmt.Println("root文件夹创建完成")
	}

	md5, err := client.Upload("image", "d:/demo.jpg")
	if err != nil {
		return
	}
	fmt.Println(md5)

	url := client.GetViewURL(md5, 50)
	fmt.Println(url)
}
```