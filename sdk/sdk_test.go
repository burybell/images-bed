package sdk

import (
	"fmt"
	"testing"
)

const IP = "127.0.0.1"

func TestScene(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	scene := client.CreateScene("default")
	fmt.Println(scene)
}

func TestSceneList(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	scene, err := client.GetScene()
	if err != nil {
		return
	}
	fmt.Println(scene)
}

func TestUpload(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	scene, err := client.Upload("default", "C:\\Users\\LINHU\\Downloads\\im\\20150425014637913.jpg")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(scene)
}

func TestUploadView(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	url := client.GetViewURL("8c8e6db2bf7ca23248b3fa6708b1bddd", 75)
	fmt.Println(url)
}

func TestInfo(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	info := client.GetImageInfo("b737ac2948032c943f0c6972aa42fbc0")
	fmt.Println(info)
}

func TestUploadDownload(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	url := client.GetDownloadURL("b737ac2948032c943f0c6972aa42fbc0")
	fmt.Println(url)
}

func TestDownload(t *testing.T) {
	client := NewImagesBedClient("http", IP, 8848)
	url := client.Download("b737ac2948032c943f0c6972aa42fbc0", "d:/", "颗粒.jpg")
	fmt.Println(url)
}

func TestSearch(t *testing.T) {

	client := NewImagesBedClient("http", IP, 8848)
	search, err := client.Search("20")
	if err != nil {
		return
	}

	for i := range search {
		fmt.Println(search[i])
	}

}
