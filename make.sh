#!/bin/sh

go build -o bin/images-fs app.go
# shellcheck disable=SC2181
if [ "$?" = "0" ]; then
    echo "build success!"
else
    echo "build failed!"
fi