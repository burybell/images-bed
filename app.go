package main

import (
	"flag"
	"fmt"
	"gitee.com/burybell/images-bed/control"
	"net/http"
	"strings"
)

var (
	BasePath = flag.String("base", "/root/images-bed/data", "数据路径")
	//BasePath = flag.String("base", "D:\\storage\\data", "数据路径")
	Addr   = flag.String("addr", ":8848", "服务器监听地址")
	Suffix = flag.String("types", "png,jpg,jpeg,gif", "图片类型")
)

func main() {
	if Suffix != nil && *Suffix != "" {
		suffixes := strings.Split(*Suffix, ",")
		app := control.Run(*BasePath, suffixes)
		err := http.ListenAndServe(*Addr, app)
		if err != nil {
			fmt.Println("start fail")
		}
	} else {
		app := control.Run(*BasePath, nil)
		err := http.ListenAndServe(*Addr, app)
		if err != nil {
			fmt.Println("start fail")
		}
	}
}
