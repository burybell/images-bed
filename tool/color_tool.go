package tool

import (
	"image"
	"image/color"
)

type ColorCallback func(color.Color) color.Color

func ColorDell(before image.Image, callback ColorCallback) image.Image {
	after := image.NewRGBA(before.Bounds())
	width, height := after.Bounds().Max.X, after.Bounds().Max.Y
	for i := 0; i < width; i++ {
		for j := 0; j < height; j++ {
			after.Set(i, j, callback(before.At(i, j)))
		}
	}
	return after
}

func ReverseColor(before color.Color) color.Color {
	r, g, b, a := before.RGBA()
	r /= 255
	g /= 255
	b /= 255
	a /= 255
	return color.RGBA{
		R: RgbaMax - uint8(r),
		G: RgbaMax - uint8(g),
		B: RgbaMax - uint8(b),
		A: RgbaMax - uint8(a),
	}
}

func GrayColor(before color.Color) color.Color {
	r, g, b, a := before.RGBA()
	r /= 255
	g /= 255
	b /= 255
	a /= 255
	gray := (r*30 + g*59 + b*11) / 100
	return color.RGBA{
		R: uint8(gray),
		G: uint8(gray),
		B: uint8(gray),
		A: RgbaMax,
	}
}
