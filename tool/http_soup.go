package tool

import (
	"encoding/json"
	"net/http"
)

func ServeJson(writer http.ResponseWriter, code int, data interface{}) {
	writer.Header().Set("content-type", "application/json")
	marshal, err := json.Marshal(data)
	if err != nil {
		writer.WriteHeader(http.StatusServiceUnavailable)
	}
	_, err = writer.Write(marshal)
	if err != nil {
		writer.WriteHeader(http.StatusServiceUnavailable)
	}
	writer.WriteHeader(code)
}
