package tool

import (
	"fmt"
	"path"
	"testing"
)

func TestGetUniqueName(t *testing.T) {
	name := GetUniqueName(10, 4)
	fmt.Println(name)
}

func TestGetPath(t *testing.T) {
	fmt.Println(GetPathWithBase("d:/storage", "dd", 8))
}

func TestName(t *testing.T) {
	fmt.Println(path.Base("d:/storage/sfsdf/1.png"))
}
