package fs

import "errors"

var (
	SceneNotExist       = errors.New("SceneNotExist")
	ImageTypeNotSupport = errors.New("ImageTypeNotSupport")
	NullPtr             = errors.New("NullPtr")
)
